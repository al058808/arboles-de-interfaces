/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 *
 * @author victhor_brito
 */
public class Arboles {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
   int dato;
int nombre;
ArbolBinario miArbol = new ArbolBinario();
miArbol.agregarNodo(20, "veinte" );
miArbol.agregarNodo(35, "treinta y cinco" );
miArbol.agregarNodo(3, "tres" );
miArbol.agregarNodo(6, "seis" );
miArbol.agregarNodo(7, "Siete" );
miArbol.agregarNodo(58, "cincuenta y ocho" );
miArbol.agregarNodo(2, "dos" );
miArbol.agregarNodo(17, "diecisiete" );
miArbol.agregarNodo(81, "Ochenta y uno" );
    

System.out.println("--------------inOrden");
if (!miArbol.estaVacio()){
miArbol.inOrden(miArbol.raiz);
}
    
System.out.println("--------------[Preorden]");
if (!miArbol.estaVacio()){
miArbol.preOrden(miArbol.raiz);
}
System.out.println("--------------[Posorden]");
if (!miArbol.estaVacio()){
miArbol.posOrden(miArbol.raiz);
}

    }
    }
